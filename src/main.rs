#![allow(dead_code)]

use std::io::{self, BufRead};
use std::fmt;
extern crate serde;
use serde::{Deserialize};

#[derive(Deserialize, Debug)]
struct BoardInput {
    v_walls: Vec<(usize, usize)>,
    h_walls: Vec<(usize, usize)>,
    current_player_pos: (usize, usize),
    other_player_pos: (usize, usize),
    current_player_wall_limit: u8,
    other_player_wall_limit: u8,
}


const dirs: [(i8, i8); 4] = [(1, 0), (0, 1), (-1, 0), (0, -1)];
//const inf: i32 = -1 >> 1;
const inf: i32 = 999999999;

#[derive(Debug, Clone, Copy)]
enum MoveType {
    MovePawn,
    VWall,
    HWall,
}

#[derive(Debug, Clone, Copy)]
struct Move {
    move_type: MoveType,
    x: usize,
    y: usize,
}

impl fmt::Display for Move {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{} {} {}",
            match self.move_type {
                MoveType::MovePawn => "m",
                MoveType::VWall => "v",
                MoveType::HWall => "h",
            },
            self.x, self.y
        )
    }
}

enum Legality {
    Illegal,
    Legal(Option<(u8, u8)>),
}

#[derive(Clone, Hash)]
struct Board {
    edges: [[[bool; 4]; 9]; 9],
    cp: usize,
    intersec_used: [[bool; 8]; 8],
    walls_left: [u8; 2],
    pos: [(usize, usize); 2],
    //old_pos: Vec<[(usize, usize); 2]>,
}

impl Board {
    fn switch_player(&mut self) {
        self.cp = 1 - self.cp;
    }

    fn make_move(&self, m: Move) -> Self {
        let mut b = self.clone();
        match m.move_type {
            MoveType::MovePawn => {
                b.pos[b.cp] = (m.x, m.y);
            },
            MoveType::HWall => {
                b.intersec_used[m.x][m.y] = true;
                b.edges[m.x][m.y][1] = false;
                b.edges[m.x+1][m.y][1] = false;
                b.edges[m.x][m.y+1][3] = false;
                b.edges[m.x+1][m.y+1][3] = false;
            },
            MoveType::VWall => {
                b.intersec_used[m.x][m.y] = true;
                b.edges[m.x][m.y][0] = false;
                b.edges[m.x][m.y+1][0] = false;
                b.edges[m.x+1][m.y][2] = false;
                b.edges[m.x+1][m.y+1][2] = false;
            },
        }
        b.switch_player();

        b

        //match mt {
            //MoveType::MovePawn => {
                //self.old_pos.push(self.pos);
                //self.pos[self.cp] = (x, y);
            //},
            //MoveType::v_wall => {
                //self.intersec_used[x][y] = true;
                //self.edges[x][y][1] = false;
                //self.edges[x+1][y][1] = false;
                //self.edges[x][y+1][3] = false;
                //self.edges[x+1][y+1][3] = false;
            //},
            //MoveType::h_wall => {
                //self.intersec_used[x][y] = true;
                //self.edges[x][y][0] = false;
                //self.edges[x][y+1][0] = false;
                //self.edges[x+1][y][2] = false;
                //self.edges[x+1][y+1][2] = false;
            //},
        //}
        //self.switch_player();
    }

    //fn revert_move(&mut self, m: (MoveType, usize, usize)) {
        //let (mt, x, y) = m;
        //match mt {
            //MoveType::MovePawn => {
                //self.pos = self.old_pos.pop().unwrap();
            //},
            //MoveType::v_wall => {
                //self.intersec_used[x][y] = false;
                //self.edges[x][y][1] = true;
                //self.edges[x+1][y][1] = true;
                //self.edges[x][y+1][3] = true;
                //self.edges[x+1][y+1][3] = true;
            //},
            //MoveType::h_wall => {
                //self.intersec_used[x][y] = false;
                //self.edges[x][y][0] = true;
                //self.edges[x][y+1][0] = true;
                //self.edges[x+1][y][2] = true;
                //self.edges[x+1][y+1][2] = true;
            //},
        //}
        //self.switch_player();
    //}

    fn from_input(bi: BoardInput) -> Self {
        let mut edges = [[[true; 4]; 9]; 9];
        let intersec_used = [[false; 8]; 8];
        for x in 0..9 {
            edges[x][0][3] = false;
            edges[x][8][1] = false;
        }
        for y in 0..9 {
            edges[0][y][2] = false;
            edges[8][y][0] = false;
        }
        let mut b = Board {
            edges: edges,
            intersec_used: intersec_used,
            cp: 0,
            pos: [(0, 0), (0, 0)],
            //old_pos: vec![],
            walls_left: [0, 0],
        };

        for (x, y) in bi.h_walls {
            b = b.make_move(Move {
                move_type: MoveType::HWall,
                x: x,
                y: y,
            });
        }
        for (x, y) in bi.v_walls {
            b = b.make_move(Move {
                move_type: MoveType::VWall,
                x: x,
                y: y,
            });
        }

        b.cp = 0;
        b.pos[0] = bi.current_player_pos;
        b.pos[1] = bi.other_player_pos;
        b.walls_left[0] = bi.current_player_wall_limit;
        b.walls_left[1] = bi.other_player_wall_limit;
        //b.old_pos = vec![];

        b
    }

    fn find_distance(&self, player: usize) -> Option<u8> {
        let (px, py) = self.pos[player];
        let row = 8*(1-player);
        let mut visited = [[false; 9]; 9];
        let mut stacks = [Vec::with_capacity(50), Vec::with_capacity(50)];
        stacks[0].push((px, py));
        visited[px][py] = true;
        let mut idx = 0;
        let mut dist = 0;
        while stacks[idx].len() > 0 {
            //for (x, y) in stacks[idx].clone() {
            for i in 0..stacks[idx].len() {
                let (x, y) = stacks[idx][i];
                if y == row {
                    return Some(dist as u8);
                }
                for d in 0..4 {
                    let (dx, dy) = dirs[d];
                    let x1 = (x as i8 + dx) as usize;
                    let y1 = (y as i8 + dy) as usize;
                    if self.edges[x][y][d] && !visited[x1][y1] {
                        visited[x1][y1] = true;
                        stacks[1-idx].push((x1, y1));
                    }
                }
            }
            stacks[idx].clear();
            idx = 1-idx;
            dist += 1;
        }
        None
    }

    // TODO: implement fast legality checking
    fn check_legality(&self) -> Legality {
        if let Some(d0) = self.find_distance(0) {
            if let Some(d1) = self.find_distance(1) {
                Legality::Legal(Some((d0, d1)))
            } else {
                Legality::Illegal
            }
        } else {
            Legality::Illegal
        }
    }

    fn moves(&self) -> Vec<Move> {
        let mut mvs = vec![];
        let (px, py) = self.pos[self.cp];
        let px = px as i8;
        let py = py as i8;
        for d in 0..4 {
            let (dx, dy) = dirs[d];
            if self.edges[px as usize][py as usize][d] {
                let px1 = (px + dx) as usize;
                let py1 = (py + dy) as usize;
                if self.pos[1-self.cp] != (px1, py1) {
                    mvs.push(Move {
                        move_type: MoveType::MovePawn,
                        x: px1,
                        y: py1,
                    });
                } else if self.edges[px1][py1][d] {
                    mvs.push(Move {
                        move_type: MoveType::MovePawn,
                        x: (px+2*dx) as usize,
                        y: (py+2*dy) as usize,
                    });
                } else {
                    if self.edges[px1][py1][(d+1)%4] {
                        let (dx2, dy2) = dirs[(d+1)%4];
                        mvs.push(Move {
                            move_type: MoveType::MovePawn,
                            x: (px+dx+dx2) as usize,
                            y: (py+dy+dy2) as usize,
                        });
                    }
                    if self.edges[px1][py1][(d+3)%4] {
                        let (dx2, dy2) = dirs[(d+3)%4];
                        mvs.push(Move {
                            move_type: MoveType::MovePawn,
                            x: (px+dx+dx2) as usize,
                            y: (py+dy+dy2) as usize,
                        });
                    }
                }
            }
        }
        if self.walls_left[self.cp] > 0 {
            for x in 0..8 {
                for y in 0..8 {
                    if !self.intersec_used[x][y] {
                        if self.edges[x][y][1] && self.edges[x+1][y][1] {
                            mvs.push(Move {
                                move_type: MoveType::HWall,
                                x: x,
                                y: y,
                            });
                        }
                        if self.edges[x][y][0] && self.edges[x][y+1][0] {
                            mvs.push(Move {
                                move_type: MoveType::VWall,
                                x: x,
                                y: y,
                            });
                        }
                    }
                }
            }
        }

        mvs
    }

    fn check_winner(&self) -> bool {
        let [(_, y0), (_, y1)] = self.pos;
        //false
        y0 == 8 || y1 == 0
    }
}

struct CacheCell {
}

struct Cache {
    cells: Vec<Option<CacheCell>>
}

fn negamax(b: Board, max_depth: u8, depth: u8, alpha: i32, beta: i32) -> (i32, Option<Move>) {
    match b.check_legality() {
        Legality::Illegal => (inf, None),
        Legality::Legal(opt) => {
            if b.check_winner() {
                (-inf, None)
            } else if depth == max_depth {
                let dist = match opt {
                    Some((d0, d1)) => [d0 as i32, d1 as i32],
                    None => {[
                        b.find_distance(0).unwrap() as i32,
                        b.find_distance(1).unwrap() as i32,
                    ]},
                };

                (dist[1-b.cp] - dist[b.cp], None)

            } else {
                let mut best_score = -inf;
                let mvs = b.moves();
                let mut best_move = mvs[0];
                let mut alpha = alpha;
                for mv in mvs {
                    let bf = b.make_move(mv);
                    let (s, _) = negamax(bf, max_depth, depth + 1, -beta, -alpha);
                    let s = -s;
                    if s > best_score {
                        best_score = s;
                        best_move = mv;
                    }
                    if best_score > alpha {
                        alpha = best_score;
                    }
                    if alpha >= beta {
                        break;
                    }
                }

                (best_score, Some(best_move))
            }
        }
    }
}


fn main() {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let bi = serde_json::from_str(&line.unwrap()).expect("invalid json");
        let b = Board::from_input(bi);
        eprintln!("distance: {} steps", b.find_distance(b.cp).unwrap());
        let (_, mv) = negamax(b, 5, 0, -inf, inf);
        let mv = mv.unwrap();
        eprintln!("best move: {}", mv);
        println!("{}", mv);
    }
}
